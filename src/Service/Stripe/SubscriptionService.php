<?php

namespace App\Service\Payment\Stripe;

use App\Entity\StripeSubscription;
use App\Exception\CacheBlockerException;
use App\Exception\Payment\StripeApiException;
use App\Utils\Blocker\Blocker;
use Stripe\Subscription;

class SubscriptionService
{
    public function __construct(
        private readonly StripeApiService $stripeApiService,
        private readonly Blocker          $blocker,
    ) {}

    /**
     * @param string $stripeCustomerId
     * @param string $stripePriceId
     *
     * @return Subscription
     * @throws StripeApiException
     * @throws CacheBlockerException
     */
    public function createSubscription(string $stripeCustomerId, string $stripePriceId): Subscription
    {
        $this->blocker->block($stripeCustomerId, 'create', $stripePriceId);

        try {
            $stripeSubscription = $this->stripeApiService->createSubscription(
                $stripeCustomerId,
                [$stripePriceId],
            );
        } catch (StripeApiException $e) {
            $this->blocker->unblock($stripeCustomerId);

            throw new StripeApiException($e);
        }

        return $stripeSubscription;
    }

    /**
     * @param StripeSubscription $subscription
     *
     * @return void
     * @throws CacheBlockerException
     * @throws StripeApiException
     */
    public function cancelSubscription(StripeSubscription $subscription): void
    {
        $stripeCustomerId = $subscription->getClient()->getStripeCustomerId();
        $stripePriceId = $subscription->getPlan()->getGatewayPriceId();
        $stripeSubscriptionId = $subscription->getGatewaySubscriptionId();

        $this->blocker->block($stripeCustomerId, 'cancel', $stripePriceId);

        try {
            $this->stripeApiService->cancelSubscription($stripeSubscriptionId);
        } catch (StripeApiException $e) {
            $this->blocker->unblock($stripeCustomerId);

            throw new StripeApiException($e);
        }
    }

    /**
     * @param StripeSubscription $subscription
     *
     * @return void
     * @throws StripeApiException
     * @throws CacheBlockerException
     */
    public function addSubscriptionCancellation(StripeSubscription $subscription): void
    {
        $stripeCustomerId = $subscription->getClient()->getStripeCustomerId();
        $stripePriceId = $subscription->getPlan()->getGatewayPriceId();
        $stripeSubscriptionId = $subscription->getGatewaySubscriptionId();

        $this->blocker->block($stripeCustomerId, 'cancel', $stripePriceId);

        try {
            $this->stripeApiService->addSubscriptionCancellation($stripeSubscriptionId);
        } catch (StripeApiException $e) {
            $this->blocker->unblock($stripeCustomerId);

            throw new StripeApiException($e);
        }
    }

    /**
     * @param StripeSubscription $subscription
     *
     * @return void
     * @throws StripeApiException
     * @throws CacheBlockerException
     */
    public function removeSubscriptionCancellation(StripeSubscription $subscription): void
    {
        $stripeCustomerId = $subscription->getClient()->getStripeCustomerId();
        $stripePriceId = $subscription->getPlan()->getGatewayPriceId();
        $stripeSubscriptionId = $subscription->getGatewaySubscriptionId();

        $this->blocker->block($stripeCustomerId, 'restore', $stripePriceId);

        try {
            $this->stripeApiService->removeSubscriptionCancellation($stripeSubscriptionId);
        } catch (StripeApiException $e) {
            $this->blocker->unblock($stripeCustomerId);

            throw new StripeApiException($e);
        }
    }
}
