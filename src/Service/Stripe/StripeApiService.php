<?php

namespace App\Service\Payment\Stripe;

use App\DTO\Stripe\CustomerDTO;
use App\DTO\Stripe\PriceDTO;
use App\Exception\Payment\StripeApiException;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Price;
use Stripe\Stripe;
use Stripe\StripeClient;
use Stripe\Subscription;

class StripeApiService
{
    private StripeClient $client;
    private Serializer $serializer;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        Stripe::setApiKey($parameters['secretKey']);
        $this->client = new StripeClient($parameters['secretKey']);
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @return StripeClient
     */
    public function getClient(): StripeClient
    {
        return $this->client;
    }

    /**
     * @param string $customerId
     * @param string $paymentMethodId
     * @param int $amount
     * @param array $metadata
     *
     * @return PaymentIntent
     * @throws StripeApiException
     */
    public function createManualPaymentIntent(string $customerId, string $paymentMethodId, int $amount, array $metadata = []): PaymentIntent
    {
        try {
            $invoice = $this->client->invoices->create([
                'auto_advance' => true,
                'customer' => $customerId,
                'pending_invoice_items_behavior' => 'exclude',
            ]);

            $this->client->invoiceItems->create([
                'customer' => $customerId,
                'amount' => $amount,
                'currency' => 'usd',
                'invoice' => $invoice->id,
            ]);

            // finalize Invoice and create PaymentIntent
            $invoice = $this->client->invoices->finalizeInvoice($invoice->id, []);

            $paymentIntent = $this->client->paymentIntents->update($invoice->payment_intent, [
                'metadata' => $metadata,
                'payment_method' => $paymentMethodId,
                'setup_future_usage' => 'off_session', // @link https://stripe.com/docs/payments/payment-intents#future-usage
            ]);

            return $this->client->paymentIntents->confirm($paymentIntent->id, [
                'payment_method' => $paymentMethodId,
                'expand' => ['invoice'],
            ]);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }

    /**
     * @param string $customerId
     * @param array $priceIds
     * @param array $metadata
     *
     * @return Subscription
     * @throws StripeApiException
     */
    public function createSubscription(string $customerId, array $priceIds, array $metadata = []): Subscription
    {
        try {
            $items = [];
            foreach ($priceIds as $priceId) {
                $items[] = ['price' => $priceId];
            }

            $metadata = array_merge($metadata, ['customerId' => $customerId]);

            return $this->client->subscriptions->create([
                'customer' => $customerId,
                'items' => $items,
                'metadata' => $metadata,
                'expand' => ['latest_invoice.payment_intent'],
            ]);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }

    /**
     * @param string $subscriptionId
     *
     * @return Subscription
     * @throws StripeApiException
     */
    public function cancelSubscription(string $subscriptionId): Subscription
    {
        try {
            return $this->client->subscriptions->cancel($subscriptionId, []);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }

    /**
     * @param string $subscriptionId
     *
     * @return Subscription
     * @throws StripeApiException
     */
    public function retrieveSubscription(string $subscriptionId): Subscription
    {
        try {
            return $this->client->subscriptions->retrieve($subscriptionId, []);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }

    /**
     * @param string $subscriptionId
     *
     * @return Subscription
     * @throws StripeApiException
     */
    public function addSubscriptionCancellation(string $subscriptionId): Subscription
    {
        try {
            return $this->client->subscriptions->update($subscriptionId, [
                'cancel_at_period_end' => true,
            ]);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }

    /**
     * @param string $subscriptionId
     *
     * @return Subscription
     * @throws StripeApiException
     */
    public function removeSubscriptionCancellation(string $subscriptionId): Subscription
    {
        try {
            return $this->client->subscriptions->update($subscriptionId, [
                'cancel_at_period_end' => false,
            ]);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }

    /**
     * @param CustomerDTO $customerDTO
     *
     * @return Customer
     * @throws StripeApiException
     */
    public function createCustomer(CustomerDTO $customerDTO): Customer
    {
        try {
            $array = $this->serializer->toArray($customerDTO);

            return $this->client->customers->create($array);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }

    /**
     * @param PriceDTO $priceDTO
     *
     * @return Price
     * @throws StripeApiException
     */
    public function createPrice(PriceDTO $priceDTO): Price
    {
        try {
            $array = $this->serializer->toArray($priceDTO);

            return $this->client->prices->create($array);
        } catch (ApiErrorException $e) {
            throw new StripeApiException($e);
        }
    }
}
